<?php

    header("Content-type: text/csv");
    header("Content-Disposition: attachment; filename=file.csv");
    header("Pragma: no-cache");
    header("Expires: 0");

    $array = array(    // данные которые мы будем выводить в csv файл
      0 => array('Nikolay','nikolay@email.ru'),
      1 => array('Maria', 'maria@email.ru')
    );

    $out = fopen('php://output', 'w');
    
    foreach($array as $item)
    { 
      fputcsv($out, array($item[0],$item[1]));
    } 
    
    fclose($out);
    


?>