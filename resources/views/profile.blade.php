@extends('layouts.app')
@section('title', 'Личный кабинет' )
@section('content')

  <div class="page-content page-article">
    <div class="container page-content-row">
      <div class="breadcrumb">
        <ul>
          <li>
            <a href="/">Главная</a>
          </li>
          <li>
          Личный кабинет
          </li>
        </ul>
      </div>
      @if (Auth::user())
      <div class="page-content-title title">Личный кабинет</div>
        <div class="page-content-body page-article-body">
          <p>Здравствуйте, {{Auth::user()->name}}!</p>
          @if (count($orders) > 0)
          <p>Ваши заказы</p>
            <table class="table">
              <tr>
                <th>#</th>
                <th>Дата</th>
                <th>Адрес</th>
                <th>Стоимость</th>
              </tr>
              @foreach ($orders as $order)
              <tr>
                <td>{{$order->id}}</td>
                <td>{{$order->created_at->format('d.m.Y')}}</td>
                <td>{{$order->address}}</td>
                <td>{{$order->total}} руб.</td>
              </tr>
              @endforeach
            </table>
          @else
          <p>У вас пока-что нет заказов.</p>
          @endif
          <br>
          <br>
          <br>
        </div>
      @else
        <div class="page-content-title title">Зрегистрируйтесь или войдите</div>
        <div class="page-content-body page-article-body">
          <p><a href="#login">Войти</a></p>
        </div>
        <br>
        <br>
        <br>
      @endif

    </div>
  </div>

@endsection