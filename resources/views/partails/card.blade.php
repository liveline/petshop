
              <div class="catalog-item">
                <div class="catalog-stars">
                  <div class="icon icon-star catalog-star catalog-star--active"></div>
                  <div class="icon icon-star catalog-star catalog-star--active"></div>
                  <div class="icon icon-star catalog-star catalog-star--active"></div>
                  <div class="icon icon-star catalog-star catalog-star--active"></div>
                  <div class="icon icon-star catalog-star catalog-star--active"></div>
                </div>
                <div class="catalog-labels">
                  @php 
                    $attrs = App\ProductAttribute::where('product_id','=',$product->id)->get();
                    foreach ($attrs as $att) {
                      if ($att->attribute_id == 9) {
                        $type = $att->attribute_val->attribute_value;
                      }
                      if ($att->attribute_id == 6) {
                        $weight = $att->attribute_val->attribute_value;
                      }
                    }
                  @endphp
                  @if (isset($type))
                  <span class="red">{{$type}}</span>
                  @endif
                </div>
                <div class="catalog-image">
                  <a href="/product/{{$product->id}}"><img src="/storage/{{ json_decode($product->images)[0]}}" alt=""></a>
                </div>
                <div class="catalog-title">
                  <a href="/product/{{$product->id}}">{{$product->title}}</a> 
                  @if (isset($weight))
                  <span class="red">{{$weight}}</span>
                  @endif
                </div>
                @if ($product->sale == 1)
                <div class="catalog-price">
                  <em style="font-size: 14px;text-decoration: line-through;">{{$product->price}} <span>й</span></em> {{$product->price - ($product->price * $product->sale_size / 100) }} <span>й</span>
                </div>
                @else
                <div class="catalog-price">
                  {{$product->price}} <span>й</span>
                </div>
                @endif
                <form action="javascript:void(0)" method="POST" class="form-cart">
                  <div class="catalog-qty">
                    <div class="product-qty">
                      <span class="product-qty-minus"></span><input type="text" value="1" name="qty"><span class="product-qty-plus"></span>
                    </div>
                  </div>
                  <div class="catalog-button">
                      <a href="javascript:void(0)"><span>В корзину</span><i class="icon icon-plus"></i></a>
                      <input type="hidden" name="product" value="{{$product->title}}">
                      <input type="hidden" name="price" value="{{$product->price}}">
                      <input type="hidden" name="id" value="{{$product->id}}">
                      @csrf
                  </div>
                </form>
              </div>