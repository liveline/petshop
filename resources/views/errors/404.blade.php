@extends('layouts.app')
@section('title', '404 - ничего не найдено' )
@section('content')

  <div class="page-404">
    <div class="container page-404-row">
      <div class="page-404-left">
        <div class="page-404-title">404</div>
        <div class="page-404-subtitle">страница не найдена</div>
      </div>
      <div class="page-404-right">
        <div class="icon icon-404"></div>
      </div>
    </div>
  </div>
@endsection