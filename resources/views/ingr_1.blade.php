@extends('layouts.app')
@section('title', 'Ингридиенты, которые мы используем')
@section('content')

  <div class="page-content page-article">
    <div class="container page-content-row">
      <div class="breadcrumb">
        <ul>
          <li>
            <a href="/">Главная</a>
          </li>
          <li>
          Ингридиенты которые мы используем
          </li>
        </ul>
      </div>

      <div class="page-content-title title">Ингридиенты которые мы используем</div>
      <div class="page-content-about about">
        <div class="container">
          <div class="page-content-body page-article-body">
            <p>Заботясь о здоровье ваших питомцев, мы используем для производства кормов «Petdiets» мясо, овощи и рис, которые являются максимально полезными. Абсолютно все ингредиенты имеют сертификаты качества, которые дополнительно контролируются нашими специалистами перед отправкой на производство. Мы предлагаем вам ознакомиться с перечнем используемых мясных и овощных продуктов, а также их полезными свойствами. </p>
          </div> 
          <div class="about-list ingr-list">
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/goods%20for%20using/1_1.jpg" alt=""></div>
              <div class="about-title">Мясо</div>
              <div class="about-text">
                <p>Мясной белок – полноценный, то есть содержит все незаменимые аминокислоты, необходимые для собак. Помимо этого в мясе содержится большое количество фосфора, железа, цинка которые улучшают пищеварительный процесс и повышают выносливость и здоровье сердца. </p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/goods%20for%20using/_8.jpg" alt=""></div>
              <div class="about-title">Субпродукты</div>
              <div class="about-text">
                <p>В сердце содержится в двое больше эластина и коллагенов, необходимых для суставов. Рубец содержит полезные пищеварительные ферменты. Печень богата витаминов А, необходимым для окислительно-оздоровительных процессов.</p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/goods%20for%20using/_5.jpg" alt=""></div>
              <div class="about-title">Морковь</div>
              <div class="about-text">
                <p>Является источником витамина А, антиоксиданков и бета-каротина. Большую ценность представляет волокнистая структура продукта, переваривание которой поддерживает здоровое состояние желудочно-кишечного тракта. </p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/goods%20for%20using/_2.jpg" alt=""></div>
              <div class="about-title">Брокколи</div>
              <div class="about-text">
                <p>Полно кальцием, который способствует укреплению костей животного. Также является источником калия, положительно влияющего на функционирование организма в целом, и клетчатки, которая выводит шлаки и токсины.</p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/goods%20for%20using/_10.jpg" alt=""></div>
              <div class="about-title">Цуккини</div>
              <div class="about-text">
                <p>Богат витаминами А, С, Е, РР и группы В. На фоне минеральных веществ, которые есть в этом ингредиенте, преобладает калий. Однако также следует отметить наличие кальция, натрия, магния, фосфора и железа.</p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/goods%20for%20using/_9.jpg" alt=""></div>
              <div class="about-title">Тыква</div>
              <div class="about-text">
                <p>Является источником витаминов группы В и С. В нем содержится каротин, медь, фосфор, железо. Незаменима в вопросах вывода из организма холестерина благодаря наличию пектина. Способствует восстановлению антиоксидантных функций. </p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/goods%20for%20using/_12.jpg" alt=""></div>
              <div class="about-title">Яблоки</div>
              <div class="about-text">
                <p>Являются источником витамином С, А, В, С и К, а также множества других уникальных антиоксидантных и ботанических соединений, в том числе таких как флавоноиды. Яблоки содержат растворимые и нерастворимые волокна, положительно влияющие на работу желудочно-кишечного тракта.</p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/goods%20for%20using/1_2.jpg" alt=""></div>
              <div class="about-title">Шпинат</div>
              <div class="about-text">
                <p>В его листьях содержится большое количество белка, железа, жирных кислот омега-3, меди, а также необходимых витаминов А, В2, В6, С, Е и К. Положительно влияет на пищеварение, поддерживает работу сердца, предотвращает воспалительные процессы. </p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/goods%20for%20using/_7.jpg" alt=""></div>
              <div class="about-title">Петрушка</div>
              <div class="about-text">
                <p>Положительным образом влияет на иммунную систему благодаря содержанию витаминов А и С. Хлорофилл, присутствующий в ее листьях, подавляет действие вредных бактерий</p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/goods%20for%20using/_1.jpg" alt=""></div>
              <div class="about-title">Крапива</div>
              <div class="about-text">
                <p>Является источником витаминов группы В и С. В нем содержится каротин, медь, фосфор, железо. Незаменима в вопросах вывода из организма холестерина благодаря наличию пектина. Способствует восстановлению антиоксидантных функций. </p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/goods%20for%20using/_3.jpg" alt=""></div>
              <div class="about-title">Семена льна</div>
              <div class="about-text">
                <p>Является источником растворимой и нерастворимой клетчатки, аминокислот, омега-6 и что более важно Омега-3. Богатейший источник альфа-линолевая кислота (ALA). Способствует улучшению пищеварения.</p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/goods%20for%20using/_6.jpg" alt=""></div>
              <div class="about-title">Оливковое масло</div>
              <div class="about-text">
                <p>В состав входят уникальные по своим свойствам мононенасыщенные и полиненасыщенные жирные кислоты. Положительно влияет на органы пищеварения и состояние сердечно-сосудистой системы.</p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/goods%20for%20using/_4.jpg" alt=""></div>
              <div class="about-title">Люцерна</div>
              <div class="about-text">
                <p>Является источником восьми аминокислот, витаминов А, К, Е, Д и В6. Также в этом растении содержится фосфор и кальций. Его добавление в корма способствует улучшению аппетита животного, а также является профилактикой по предотвращению желудочно-кишечных заболеваний.</p>
              </div>
            </div>
          </div>
          <br>
          <br>
          <br>
        </div>
      </div>

      <div class="catalog">
        <div class="container">
          <div class="catalog-tabs">
            <div class="catalog-tabs-nav">
              <div class="catalog-tabs-nav-title title">Возможно вас заинтересуют эти товары</div>
            </div>
            <div class="catalog-arrows">
              <div class="catalog-arrow catalog-arrow-left">
                <div class="icon icon-arrow-l"></div>
              </div>
              <div class="catalog-arrow catalog-arrow-right">
                <div class="icon icon-arrow-r"></div>
              </div>
            </div>
            <div class="catalog-tabs-list">
              <div class="catalog-tabs-item catalog-tabs-item--1 active">
                <div class="catalog-list catalog-list-4">
                @foreach ($popular as $product)
                  @include('partails.card', ['product' => $product])
                @endforeach
                </div>
                <br>
                <br>
                <br>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection