@extends('layouts.app')
@section('title', 'Ингридиенты, которые мы не используем')
@section('content')

  <div class="page-content page-article">
    <div class="container page-content-row">
      <div class="breadcrumb">
        <ul>
          <li>
            <a href="/">Главная</a>
          </li>
          <li>
          Ингридиенты которые мы не используем
          </li>
        </ul>
      </div>

      <div class="page-content-title title">Ингридиенты которые мы не используем</div>
      <div class="page-content-about about">
        <div class="container">
          <div class="page-content-body page-article-body">
            <p>Мы категорически не используем при приготовлении кормов следующие виды продуктов.</p>
          </div> 
          <div class="about-list ingr-list">
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/+.jpg" alt=""></div>
              <div class="about-title">Кукуруза</div>
              <div class="about-text">
                <p>Может являться причиной некоторых проблем для собак. Плохо переваривается и содержит клейковину, которая зачастую является причиной возникновения аллергии. </p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/5.jpg" alt=""></div>
              <div class="about-title">Свинина</div>
              <div class="about-text">
                <p>Содержит большое количество жира и чаще чем у других травоядных животных бывает поражено глистами и паразитами. Крайне небезопасно давать в сыром виде.</p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/3.jpg" alt=""></div>
              <div class="about-title">Пшеница</div>
              <div class="about-text">
                <p>Часто является общим наполнителем в кормах в силу низкой стоимости. Тем не менее, есть существенные доказательства, что пшеница как и кукуруза содержит много подобных аллергенов, которые могут вызвать зуд, плохое пищеварение, понос, плохую абсорбции и зловонный стул.</p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/8.jpg" alt=""></div>
              <div class="about-title">Соя</div>
              <div class="about-text">
                <p>Эстрогены сои могут являться разрушителями эндокринной системы. В дополнение к фитоэстрогенов, соя также содержит фитаты, которые мешают усвоению минеральных веществ, блокируют ферменты, необходимые для переваривания белков</p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/4.jpg" alt=""></div>
              <div class="about-title">Сахар</div>
              <div class="about-text">
                <p>Доказано, что сахар плохо влияет на здоровье глаз и зрение собаки. Способствует ожирению порче зубов и появлению зуда.</p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/6.jpg" alt=""></div>
              <div class="about-title">Соль</div>
              <div class="about-text">
                <p>Может являться причиной моче-каменной болезни и других расстройств организма. При сбалансированном питании, количество содержащихся в рационе минеральные веществ и солей является достаточным для собак</p>
              </div>
            </div>
            <div class="about-item">
              <div class="about-icon"><img src="http://petdiets.ru/image/data/system/goods/7.jpg" alt=""></div>
              <div class="about-title">Специи</div>
              <div class="about-text">
                <p>Могут вызвать расстройство желудка, раздражение слизистых оболочек, нарушение обоняния. </p>
              </div>
            </div>
          </div>
          <br>
          <br>
          <br>
        </div>
      </div>

      <div class="catalog">
        <div class="container">
          <div class="catalog-tabs">
            <div class="catalog-tabs-nav">
              <div class="catalog-tabs-nav-title title">Возможно вас заинтересуют эти товары</div>
            </div>
            <div class="catalog-arrows">
              <div class="catalog-arrow catalog-arrow-left">
                <div class="icon icon-arrow-l"></div>
              </div>
              <div class="catalog-arrow catalog-arrow-right">
                <div class="icon icon-arrow-r"></div>
              </div>
            </div>
            <div class="catalog-tabs-list">
              <div class="catalog-tabs-item catalog-tabs-item--1 active">
                <div class="catalog-list catalog-list-4">
                @foreach ($popular as $product)
                  @include('partails.card', ['product' => $product])
                @endforeach
                </div>
                <br>
                <br>
                <br>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection