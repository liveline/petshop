<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="ru" xml:lang="ru"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Путевые листы</title>
<!--<base href="https://petdiets.ru/admin/">--><base href=".">
<link rel="stylesheet" type="text/css" href="/css/invoice.css">
</head>
<body>
@php
  $price = 0;
  Cart::destroy();
@endphp
@foreach ($orders as $order)
<div>
  <h1>Путевой лист {{$order->order_id}}</h1>
<div>
  <b>№ заказа:</b>{{$order->order_id}};
  <br><b>Телефон:</b> {{$order->phone}};
  <b>Адрес доставки:</b> {{$order->name}}, {{$order->address}}, <b>Комментарий:</b> {{$order->comment}}
      
  <table>
  <table class="product">
    <tbody><tr class="heading">
      <td><b>Товар</b></td>
      <td align="right"><b>Количество</b></td>
      <td align="right"><b>Цена за единицу</b></td>
      <td align="right"><b>Итого</b></td>
    </tr>

      @php
        Cart::restore($order->order_id);
      @endphp
      @foreach (Cart::content() as $row)
      <tr>
        <td>{{$row->name}}</td>
        <td align="right">{{$row->qty}}</td>
        <td align="right">{{$row->price}} р.</td>
        <td align="right">{{$row->price * $row->qty}} р.</td>
      </tr>
      @endforeach
      <tr>
        <td align="right" colspan="3"><b>Сумма:</b></td>
        <td align="right">{{$order->total}} р.</td>
      </tr>
      <tr>
      <td align="right" colspan="3"><b>{{$order->delivery_type}}</b></td>
        <td align="right">{{$order->delivery_price}} р.</td>
      </tr>
      @if ($order->order_sale > 0)
      <tr>
      <td align="right" colspan="3"><b>Скидка</b></td>
        <td align="right">{{$order->total * $order->order_sale / 100}} р.</td>
      </tr>
      @endif
      <td align="right" colspan="3"><b>Итого:</b></td>
        <td align="right">{{$order->total_price}} р.</td>
      </tr>
      </tbody></table>
      <table class="product">
      </table>
  </div>
  @php
  $price = $price + $order->total_price;
  Cart::destroy();
  @endphp
@endforeach
<h2 style="text-align: center;">ИТОГО: {{$price}} р.</h2>
</body></html>