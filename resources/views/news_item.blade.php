@extends('layouts.app')
@section('title', $news->title )
@section('content')

  <div class="page-content page-article">
    <div class="container page-content-row">
      <div class="breadcrumb">
        <ul>
          <li>
            <a href="/">Главная</a>
          </li>
          <li>
          <a href="/news">Блог</a>
          </li>
          <li>
          {{$news->title}}
          </li>
        </ul>
      </div>
      <div class="page-content-title title">{{$news->title}}</div>
      <div class="news">
        <div class="news-list">
            <div class="news-item" style="width: 100%;">
              <div class="news-content" style="width: 100%;">
                <div class="news-text" style="min-height: 1px;padding-bottom: 20px;">
                  {!!$news->text!!}
                </div>
              </div>
            </div>
        </div>
        <br>
      </div>
      <div class="catalog">
        <div class="container">
          <div class="catalog-tabs">
            <div class="catalog-tabs-nav">
              <div class="catalog-tabs-nav-title title">Возможно вас заинтересуют эти товары</div>
            </div>
            <div class="catalog-arrows">
              <div class="catalog-arrow catalog-arrow-left">
                <div class="icon icon-arrow-l"></div>
              </div>
              <div class="catalog-arrow catalog-arrow-right">
                <div class="icon icon-arrow-r"></div>
              </div>
            </div>
            <div class="catalog-tabs-list">
              <div class="catalog-tabs-item catalog-tabs-item--1 active">
                <div class="catalog-list catalog-list-4">
                @foreach ($popular as $product)
                  @include('partails.card', ['product' => $product])
                @endforeach
                </div>
                <br>
                <br>
                <br>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection