@extends('layouts.app')
@section('title', $page->seo_title )
@section('content')

  <div class="page-content page-article">
    <div class="container page-content-row">
      <div class="breadcrumb">
        <ul>
          <li>
            <a href="/">Главная</a>
          </li>
          <li>
          {{$page->title}}
          </li>
        </ul>
      </div>
      {!!$page->body!!}
      
      <div class="catalog">
        <div class="container">
          <div class="catalog-tabs">
            <div class="catalog-tabs-nav">
              <div class="catalog-tabs-nav-title title">Возможно вас заинтересуют эти товары</div>
            </div>
            <div class="catalog-arrows">
              <div class="catalog-arrow catalog-arrow-left">
                <div class="icon icon-arrow-l"></div>
              </div>
              <div class="catalog-arrow catalog-arrow-right">
                <div class="icon icon-arrow-r"></div>
              </div>
            </div>
            <div class="catalog-tabs-list">
              <div class="catalog-tabs-item catalog-tabs-item--1 active">
                <div class="catalog-list catalog-list-4">
                @foreach ($popular as $product)
                  @include('partails.card', ['product' => $product])
                @endforeach
                </div>
                <br>
                <br>
                <br>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection