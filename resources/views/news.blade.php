@extends('layouts.app')
@section('title', 'Блог' )
@section('content')
  <div class="page-content page-article">
    <div class="container page-content-row">
      <div class="breadcrumb">
        <ul>
          <li>
            <a href="/">Главная</a>
          </li>
          <li>
          Блог
          </li>
        </ul>
      </div>
      <div class="page-content-title title">Блог</div>
      <div class="news">
        <div class="news-list">
          @foreach ($news as $n)
            <div class="news-item" style="width: 100%;">
              <div class="news-content" style="width: 100%;">
                <div class="news-top">
                  <div class="news-title"><a href="/news/{{$n->id}}">{{$n->title}}</a></div>
                </div>
                <div class="news-text" style="min-height: 1px;padding-bottom: 20px;">
                  {!!\Illuminate\Support\Str::words($n->text,64)!!}
                </div>
                <div class="news-bottom">
                  <div class="news-more" style="margin-left: 0;"><a href="/news/{{$n->id}}">подробнее</a></div>
                </div>
              </div>
            </div>
          @endforeach
        </div>
        <br>
        <br>
        {{ $news->links() }}
        <br>
      </div>
    </div>
  </div>

@endsection