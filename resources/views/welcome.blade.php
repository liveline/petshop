@extends('layouts.app')
@section('title', 'Главная страница')
@section('content')
    
  <div class="main">
    <div class="container main-row">
      <div class="main-menu">
        <ul>

            @foreach ($cats as $c)
            <li><a href="/category/{{$c->id}}"><span class="main-menu-image"><img src="/storage/{{$c->icon}}" alt=""></span> <span class="main-menu-title">{{$c->title}}</span></a></li>
            @endforeach
            <li><a href="#ingridients" class="scrollto"><span class="main-menu-image"><img src="/media/img/broccoli-outline.png" alt=""></span> <span class="main-menu-title">Ингридиенты</span></a></li>
            <li><a href="#bottom" class="scrollto"><span class="main-menu-image"><img src="/media/img/gift.png" alt=""></span> <span class="main-menu-title">Специальные предложения</span></a></li>
        </ul>
      </div>
      <div class="main-slider">
        <div class="main-slider-item" style="background-image: url(media/img/banner_1-1.jpg);">
          <div class="main-slider-title main-slider-title--bottom white">Первый в России <br> <span>Натуральный кром для собак</span></div>
        </div>
      </div>
    </div>
  </div>
  <div class="catalog">
    <div class="container">
      <div class="catalog-tabs">
        <div class="catalog-tabs-nav">
          <ul>
            <li class="active"><a href="javascript:void(0)" data-tab="4">Популярное</a></li>
            <li><a href="javascript:void(0)" data-tab="3">Акции</a></li>
          </ul>
        </div>
        <div class="catalog-arrows">
          <div class="catalog-arrow catalog-arrow-left">
            <div class="icon icon-arrow-l"></div>
          </div>
          <div class="catalog-arrow catalog-arrow-right">
            <div class="icon icon-arrow-r"></div>
          </div>
        </div>
        <div class="catalog-tabs-list">
          <div class="catalog-tabs-item catalog-tabs-item--3">
            <div class="catalog-list catalog-list-4">
            @foreach ($sale as $product)
              @include('partails.card', ['product' => $product])
            @endforeach
            </div>
          </div>
          <div class="catalog-tabs-item catalog-tabs-item--4 active">
            <div class="catalog-list catalog-list-4">
            @foreach ($popular as $product)
              @include('partails.card', ['product' => $product])
            @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="ingridients"></div>
  <div class="adventages">
    <div class="container">
      <div class="adventages-title title">Ингредиенты</div>
      <div class="adventages-list">
        <a href="/ingredienty_kotorye_my_ispolzuem" class="adventages-item" style="width: 45%;">
          <div class="adventages-item-icon"><img src="/media/img/ing-1.jpg" alt=""></div>
          <div class="adventages-item-title">Ингредиенты, которые мы используем</div>
        </a>
        <a href="/ingredienty_kotorye_my_ne_ispolzuem" class="adventages-item" style="width: 45%;">
          <div class="adventages-item-icon"><img src="/media/img/ing-2.jpg" alt=""></div>
          <div class="adventages-item-title">Ингредиенты, которые мы не используем</div>
        </a>
      </div>
    </div>
  </div>
  <div class="banner_new" style="margin-top: 30px;margin-bottom: 50px;">
    <div class="container">
      <a href="/products"><img src="/media/img/banner_new.png" style="max-width: 100%;" alt=""></a>
    </div>
  </div>
  <div class="catalog">
    <div class="container catalog-row">
      <div class="catalog-info">
        <div class="catalog-info-icon">
          <a href="javasctip:void(0)"><div class="icon icon-homa"></div></a>
        </div>
        <div class="catalog-info-title"><a href="javasctip:void(0)">Сырые рационы</a></div>
      </div>
      <div class="catalog-list catalog-list-3">
        @foreach ($meat as $product)
          @include('partails.card', ['product' => $product])
        @endforeach      </div>
    </div>
  </div>
  <div class="catalog">
    <div class="container catalog-row">
      <div class="catalog-info">
        <div class="catalog-info-icon">
          <a href="javasctip:void(0)"><div class="icon icon-cat"></div></a>
        </div>
        <div class="catalog-info-title"><a href="javasctip:void(0)">Обработанные корма</a></div>
      </div>
      <div class="catalog-list catalog-list-3">
        @foreach ($term as $product)
          @include('partails.card', ['product' => $product])
        @endforeach
      </div>
    </div>
  </div>
  @if ($user = Auth::user())
  @else
  <div class="banner-2">
    <div class="container">
      <div class="banner-2-row">
        <div class="banner-2-image">
          <img src="media/img/smarty-cat.png" alt="">
        </div>
        <span class="banner-2-title">Простая регистрация - легкие и быстрые покупки</span>
        <span class="banner-2-button"><a href="#register">Регистрация в 2 клика</a></span>

      </div>
    </div>
  </div>
  @endif
  <div class="catalog">
    <div class="container catalog-row">
      <div class="catalog-info">
        <div class="catalog-info-icon">
          <a href="javasctip:void(0)"><div class="icon icon-dog"></div></a>
        </div>
        <div class="catalog-info-title"><a href="javasctip:void(0)">Лакомства</a></div>
      </div>
      <div class="catalog-list catalog-list-3">
        @foreach ($lac as $product)
          @include('partails.card', ['product' => $product])
        @endforeach
      </div>
    </div>
  </div>
  <div id="map" style="width: 100%;height: 400px;"></div>
@endsection