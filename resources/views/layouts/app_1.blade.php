<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>PETSHOP - @yield('title')</title>
  <link rel="stylesheet" href="/media/css/main.css">
  <link rel="stylesheet" href="/media/css/toastr.min.css">
  <script src="/media/js/jquery-3.2.1.min.js"></script>
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#733a1c">
  <meta name="theme-color" content="#ffffff">
</head>
<body>

  @yield('content')

  <script src="/media/js/slick.min.js"></script>
  <script src="/media/js/icheck.min.js"></script>
  <script src="/media/js/remodal.min.js"></script>
  <script src="/media/js/jquery.selectric.js"></script>
  <script src="/media/js/baron.min.js"></script>
  <script src="/media/js/toastr.min.js"></script>
  <script src="/media/js/jquery.sticky.js"></script>
  <script src="/media/js/custom.js"></script>


</body>
</html>