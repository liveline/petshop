@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->


                        @if(!is_null($dataTypeContent->getKey()))
                            @php
                              Cart::restore($dataTypeContent->order_id);
                            @endphp
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <p><strong>Заказ:</strong></p>
                                    <table class="table bordered">
                                        <thead>
                                            <tr>
                                                <th>Товар</th>
                                                <th>Цена</th>
                                                <th>Количество</th>
                                                <th>Итого</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach (Cart::content() as $row)
                                              <tr>
                                                <td style="vertical-align: middle;">
                                                   <a href="/admin/products/{{$row->id}}" target="_blank">{{$row->name}}</a>
                                                </td>
                                                <td style="vertical-align: middle;">{{$row->price}}</td>
                                                <td style="vertical-align: middle;">
                                                  <form action="/order/cartUpdate" class="form-inline" id="cart-{{$row->rowId}}" method="POST">
                                                    <div class="form-group">
                                                    <input type="text" name="qty" class="form-control" data-price="{{$row->price}}" value="{{$row->qty}}">
                                                    <button class="btn btn-success"><span class="icon voyager-check"></span></button>
                                                    </div>
                                                    <input type="hidden" value="{{$row->rowId}}" name="rowid">
                                                    <input type="hidden" value="{{$dataTypeContent->order_id}}" name="order_id">
                                                    @CSRF
                                                  </form>
                                                </td>
                                                <td style="vertical-align: middle;">{{$row->qty * $row->price}}</td>
                                                <td style="vertical-align: middle;">
                                                    <form action="/order/cartRemove" method="POST">
                                                        <input type="hidden" value="{{$row->rowId}}" name="id">
                                                        <input type="hidden" value="{{$dataTypeContent->order_id}}" name="order_id">
                                                        <button class="btn btn-danger"><div class="icon voyager-trash"></div></button>
                                                        @csrf
                                                    </form>
                                                </td>
                                              </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <p>Добавить товар</p>
                                    <form action="/order/cartAdd" class="form-inline" method="POST">
                                      <div class="form-group">
                                      <input type="text" name="product_id" id="findproduct" class="form-control" placeholder="ID Товара">
                                      <input type="text" name="qty" class="form-control" placeholder="Количество">
                                      <button class="btn btn-success"><span class="icon voyager-basket"></span></button>
                                      </div>
                                      <input type="hidden" value="{{$dataTypeContent->order_id}}" name="order_id">
                                      @CSRF
                                    </form>
                            <div class="row">
                                <div class="col-md-12">
                                <br>
                                <br>
                                    <p><strong>Информация о товарах</strong></p>
                                    <table class="table table-bordered" id="dataTable">
                                        <thead>
                                            <th>ID Товара</th>
                                            <th>Название</th>
                                            <th>Категория</th>
                                            <th>Цена</th>
                                        </thead>
                                        <tbody>
                                            @foreach (App\Product::all() as $product)
                                            <tr>
                                                <td>{{$product->id}}</td>
                                                <td>{{$product->title}}</td>
                                                <td>{{$product->category->title}}</td>
                                                <td>{{$product->price}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                                </div>

                                <div class="col-md-6">
                                    @php 
                                        $actualPrice = $dataTypeContent->delivery_price + Cart::subtotal(0, '.', '');
                                    @endphp
                                    @if ($dataTypeContent->total_price != $actualPrice)
                                    <div class="alert alert-danger fade in alert-dismissible">
                                        <strong>Внимание!</strong> Корзина была обновлена, обновите цену заказа.
                                    </div>
                                    @else
                                        <a href="/admin/order/invoice/{{$dataTypeContent->id}}" class="btn btn-info" target="_blank">Счет</a>
                                        <a href="/admin/order/list/{{$dataTypeContent->id}}" class="btn btn-info" target="_blank">Сводный лист</a>
                                        <a href="/admin/order/path/{{$dataTypeContent->id}}" class="btn btn-info" target="_blank">Путевой лист</a>
                                    @endif
                                    <p><strong>Стоимость заказа</strong></p>
                                    <form role="form" class="form-edit-add form-inline" action="/order/updatePrice" method="POST" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Цена товаров:</label> <br>
                                            <input type="text" class="form-control" id="orderTotal" name="total" readonly value="{{Cart::subtotal(0, '.', '')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Цена доставки:</label> <br>
                                            <input type="number" min="0" class="form-control" id="orderDeliveryPrice" value="{{$dataTypeContent->delivery_price}}" name="delivery_price">
                                        </div>
                                        <div class="form-group">
                                            <label>Скидка :</label> <br>
                                            <input type="number" min="0" max="100" class="form-control" id="orderSale" value="{{$dataTypeContent->order_sale}}" name="order_sale">
                                        </div>
                                        <div class="form-group">
                                            <label>Итого:</label> <br>
                                            <input type="text" class="form-control" id="orderTotalPrice" value="{{$dataTypeContent->delivery_price + Cart::subtotal(0, '.', '')}}" readonly name="total_price">
                                        </div>
                                        <input type="hidden" name="id" value="{{$dataTypeContent->id}}">
                                        @csrf
                                        <div class="form-group" style="padding-top: 30px;">
                                            <button class="btn btn-success"><i class="icon voyager-double-right"></i></button>
                                        </div>
                                    </form>
                                    <br>
                                    <br>
                                    <p><strong>Комментарий администратора</strong></p>
                                    <form role="form" action="/order/updateComment" method="POST" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <textarea name="admin_comment" id="" class="form-control">{{$dataTypeContent->admin_comment}}</textarea>
                                        </div>
                                        <input type="hidden" name="id" value="{{$dataTypeContent->id}}">
                                        @csrf
                                        <div class="form-group" style="padding-top: 0px;">
                                            <button class="btn btn-success">Обновить комментарий</button>
                                        </div>
                                    </form>


                    <form role="form"
                            class="form-edit-add"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                            <p>Дополнительно</p>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $options = json_decode($row->details);
                                    $display_options = isset($options->display) ? $options->display : NULL;
                                @endphp
                                @if ($options && isset($options->legend) && isset($options->legend->text))
                                    <legend class="text-{{$options->legend->align or 'center'}}" style="background-color: {{$options->legend->bgcolor or '#f0f0f0'}};padding: 5px;">{{$options->legend->text}}</legend>
                                @endif
                                @if ($options && isset($options->formfields_custom))
                                    @include('voyager::formfields.custom.' . $options->formfields_custom)
                                @else
                                    <div class="form-group @if($row->type == 'hidden') hidden @endif" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                        {{ $row->slugify }}
                                        <label for="name">{{ $row->display_name }}</label>
                                        @include('voyager::multilingual.input-hidden-bread-edit-add')
                                        @if($row->type == 'relationship')
                                            @include('voyager::formfields.relationship')
                                        @else
                                            {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                        @endif

                                        @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                        @endforeach
                                    </div>
                                @endif
                            @endforeach
                        <div class="">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>

                        @if(is_null($dataTypeContent->getKey()))
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                        @endif
                    </form>
                                </div>
                            </div>
                        </div>
                        @endif

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {}
        var $image

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });
           var table = $('#dataTable').DataTable({!! json_encode(
               array_merge([
                   "order" => [],
                   "language" => __('voyager::datatable'),
                   "columnDefs" => [['targets' => -1, 'searchable' =>  false, 'orderable' => false]],
               ],
               config('voyager.dashboard.data_tables', []))
           , true) !!});

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();

            $('#orderDeliveryPrice').on('change',function(){
                calcOrder();
            });

            $('#orderSale').on('keyup',function(){
                calcOrder();
            });
            function calcOrder(){
                var total = $('#orderTotal').val();
                var deliveryPrice = $('#orderDeliveryPrice').val();
                if (deliveryPrice == "") {
                   $('#orderDeliveryPrice').val(0) 
                } else {
                    var orderSale = $('#orderSale').val();
                    if (orderSale > 0) {
                        var orderSubtotal = parseInt(total) + parseInt(deliveryPrice);
                        var orderSaleSum = Math.round(total * orderSale / 100);
                        console.log(orderSaleSum);
                        var totalSum = orderSubtotal - orderSaleSum;
                        $('#orderTotalPrice').val(totalSum);
                    } else {
                        var totalSum = parseInt(total) + parseInt(deliveryPrice);
                        $('#orderTotalPrice').val(totalSum);
                    }
                }
            }
        });
    </script>
@stop
