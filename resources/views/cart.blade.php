@extends('layouts.app')
@section('title', 'Доставка' )
@section('content')

  <div class="page-content">
    <div class="container page-content-row">
      <div class="breadcrumb">
        <ul>
          <li>
            <a href="javascript:void(0)">Главная</a>
          </li>
          <li>
            Корзина
          </li>
        </ul>
      </div>
      @if (count(Cart::content()))
      <div class="page-cart cart">
        <div class="cart-name title">Корзина</div>
        <div class="cart-container">
        <div class="cart-table">
          <table>
            <tr>
              <th class="cart-head-title">Наименование</th>
              <th class="cart-head-name"></th>
              <th class="cart-head-price">Цена</th>
              <th class="cart-head-qty">Количество</th>
              <th class="cart-head-total">Сумма</th>
              <th class="cart-head-remove"></th>
            </tr>
            @php
              $total = 0;
            @endphp
            @foreach (Cart::content() as $row)
            @php 
              $thisProduct = App\Product::find($row->id);
              if ($thisProduct->sale == 1) {
                $sale = ($thisProduct->price * $thisProduct->sale_size)/100;
                $total = $total + ceil(($row->price - $sale) * $row->qty);
                $saleprice = ceil($row->price - $sale);
              } else {
                $total = $total + $row->price * $row->qty;
              }
              $deliveryPrice = 300;
              if ($total > 5000) {
                $deliveryPrice = 0;
              }
            @endphp
            <tr>
              <td class="cart-image"><img src="/storage/{{json_decode($thisProduct->images)[0]}}" alt=""></td>
              <td class="cart-title">{{$row->name}}</td>
              @if ($thisProduct->sale == 1)
              <td class="cart-price">{{$saleprice}} <span>Й</span></td>
              @else
              <td class="cart-price">{{$row->price}} <span>Й</span></td>
              @endif
              <td class="cart-qty">
                <form action="/cart/update" id="cart-{{$row->rowId}}" method="POST">
                  <div class="cart-qty-box">
                    <span class="product-qty-minus" data-row="{{$row->rowId}}"></span>
                    @if ($thisProduct->sale == 1)
                    <input type="text" name="qty" data-price="{{$saleprice}}" value="{{$row->qty}}">
                    @else
                    <input type="text" name="qty" data-price="{{$row->price}}" value="{{$row->qty}}">
                    @endif
                    <span class="product-qty-plus" data-row="{{$row->rowId}}"></span>
                  </div>
                  <input type="hidden" value="{{$row->rowId}}" name="rowid">
                  @CSRF
                </form>
              </td>
              @if ($thisProduct->sale == 1)
                <td class="cart-total"><i>{{$saleprice * $row->qty }}</i> <span>Й</span></td>
              @else
                <td class="cart-total"><i>{{$row->price * $row->qty }}</i> <span>Й</span></td>
              @endif
              <td class="cart-remove"><form action="/cart/remove" method="POST"><input type="hidden" value="{{$row->rowId}}" name="id"><button><i class="icon icon-x"></i></button>@csrf</form></td>
            </tr>
            @endforeach
          </table>
        </div>
        <form action="/checkout" class="delivery-form" id="checkoutForm" method="POST">
          <div class="cart-delivery">
            <div class="cart-delivery-item">
              <div class="cart-delivery-label">Доставка:</div>
              <div class="cart-delivery-content">
                <div class="cart-delivery-radio">
                    <input type="radio" id="delivery-method-1" required data-msg="Вы не выбрали способ доставки" name="delivery_method" value="В пределах МКАД"><label for="delivery-method-1">В пределах МКАД @if ($total < 5000)— 300 рублей@else — бесплатно@endif</label>
                </div>
                <div class="cart-delivery-radio">
                    <input type="radio" id="delivery-method-2" required data-msg="Вы не выбрали способ доставки" name="delivery_method" value="После МКАД — 300 рублей + 20 рублей за 1 км"><label for="delivery-method-2">После МКАД — @if ($total < 5000)300 рублей +@endif 20 рублей за 1 км</label>
                </div>
              </div>
            </div>
              <div class="cart-delivery-item delivery-km" >
                <div class="cart-delivery-label">Километров от МКАД:</div>
                <div class="cart-delivery-content">
                  <div class="cart-delivery-text">
                      <input type="text" name="km" id="cart_km" style="width: 100px;" placeholder="">
                  </div>
                </div>
              </div>
            <br>
          <div class="cart-bottom-right" style="float: none;">
            <div class="cart-bottom-weight">
              <p>Сумма товаров:</p>
            </div>
            <div class="cart-bottom-total">
              <p><span class="cart-bottom-total-price pretotal_price"><i>{{$total}}</i> <span>Й</span></span></p>
            </div>
          </div>

          <div class="cart-final-sum">
            <div class="cart-bottom-right" style="float: none; padding-top: 10px;">
              <div class="cart-bottom-weight">
                <p>Доставка</p>
              </div>
              <div class="cart-bottom-total">
                <p><span class="cart-bottom-total-price delivery_price"><i>{{$deliveryPrice}}</i> <span>Й</span></span></p>
              </div>
            </div>
            <div class="cart-bottom-right" style="float: none; padding-top: 10px;">
              <div class="cart-bottom-weight">
                <p>Итого:</p>
              </div>
              <div class="cart-bottom-total">
                <p><span class="cart-bottom-total-price total_price red" data-total="{{$total}}"><i>{{$total + $deliveryPrice}}</i> <span>Й</span></span></p>
              </div>
            </div>
          </div>
          <br>
            <div class="cart-delivery-form">
              <div class="cart-delivery-item" >
                <div class="cart-delivery-label">Адрес:</div>
                <div class="cart-delivery-content">
                  <div class="cart-delivery-text">
                      <input type="text" name="address" required placeholder="ул. Ленина 14, квартира 1">
                  </div>
                </div>
              </div>
              @if (!Auth::user())
              <div class="cart-delivery-item ">
                <div class="cart-delivery-label">Имя:</div>
                <div class="cart-delivery-content">
                  <div class="cart-delivery-text">
                      <input type="text" name="name" required placeholder="Введите ваше имя">
                  </div>
                </div>
              </div>
              @endif
              <div class="cart-delivery-item ">
                <div class="cart-delivery-label">Телефон:</div>
                <div class="cart-delivery-content">
                  <div class="cart-delivery-text">
                      <input type="text" name="phone" class="phone-field" required placeholder="Введите номер телефона">
                  </div>
                </div>
              </div>
              @if (!Auth::user())
              <div class="cart-delivery-item ">
                <div class="cart-delivery-label">E-mail:</div>
                <div class="cart-delivery-content">
                  <div class="cart-delivery-text">
                      <input type="text" name="mail" required placeholder="Введите e-mail">
                  </div>
                </div>
              </div>
              @endif
              <div class="cart-delivery-item">
                <div class="cart-delivery-label">Дополнительно:</div>
                <div class="cart-delivery-content">
                  <div class="cart-delivery-text">
                      <input type="text" name="comment" placeholder="Время доставки/комментарий">
                  </div>
                </div>
              </div>
              <div class="cart-delivery-item--address">
                <div class="cart-delivery-checkbox">
                    <input type="checkbox" id="pitomnik" name="pitomnik"><label for="pitomnik">Мы - питомник</label>
                </div>
                <div class="cart-delivery-item delivery_pet">
                  <br>
                  <p>Скидка для питомников - 5%</p>
                  <br>
                  <div class="cart-delivery-label">Название:</div>
                  <div class="cart-delivery-content">
                    <div class="cart-delivery-text">
                        <input type="text" name="pitomnik_name" placeholder="Название питомника">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
            @if ($total < 1500)
              <p class="attention">Минимальная сумма заказа 1500 рублей</p>
            @else
              <a href="javascript:void(0)" class="cart-button cart-button-link" style="width: 182px;">Оформить заказ</a>
            @endif
          <div class="cart-bottom" style="padding-top: 10px;">
            <div class="cart-bottom-left" style="text-align: right;">
                <button class="cart-button" style="width: 182px;">Оформить заказ</button>
            </div>
          </div>
          @csrf
          <input type="hidden" name="subtotal_sum" value="{{$total}}">
          <input type="hidden" id="total_sum" name="total_sum" value="{{$total + $deliveryPrice}}">
          <input type="hidden" id="delivery_sum" name="delivery_sum" data-price="{{$deliveryPrice}}" @if ($total < 5000) data-price-mkad="300" @else data-price-mkad="0"  @endif value="{{$deliveryPrice}}">
          @if (Auth::user())
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
            <input type="hidden" name="user_email" value="{{Auth::user()->email}}">
          @else
            <input type="hidden" name="user_id" value="0">
            <input type="hidden" name="user_email" value="0">
          @endif
          <div style="display: none;">
            <textarea name="body" id="" cols="30" rows="10">
              <table class="table">
                <thead>
                  <tr>
                    <th class="cart-head-title">Наименование</th>
                    <th class="cart-head-price">Цена</th>
                    <th class="cart-head-qty">Количество</th>
                    <th class="cart-head-sale">Скидка</th>
                    <th class="cart-head-total">Сумма</th>
                  </tr>
                </thead>
                @php
                  $total = 0;
                @endphp
                @foreach (Cart::content() as $row)
                @php 
                  $thisProduct = App\Product::find($row->id);
                  if ($thisProduct->sale == 1) {
                    $sale = ($thisProduct->price * $thisProduct->sale_size)/100;
                    $total = $total + ceil(($row->price - $sale) * $row->qty);
                  } else {
                    $total = $total + $row->price * $row->qty;
                  }
                @endphp
                <tbody>
                  <tr>
                    <td class="cart-title"><a href="/product/{{$thisProduct->id}}">{{$row->name}}</a></td>
                    <td class="cart-price">{{$row->price}} <span>Р</span></td>
                    <td class="cart-qty">{{$row->qty}}</td>
                    @if ($thisProduct->sale == 1)
                      <td class="cart-sale">{{$thisProduct->sale_size}}%</td>
                      <td class="cart-total">{{ceil(($row->price - $sale) * $row->qty) }} <span>Р</span></td>
                    @else
                      <td class="cart-sale">0%</td>
                      <td class="cart-total">{{$row->price * $row->qty }} <span>Р</span></td>
                    @endif
                  </tr>
                </tbody>
                @endforeach
              </table>
            </textarea>
          </div>
        </form>
      </div>

      </div>
      @else
        <div class="cart-empty">
          <div class="cart-empty-left">
            <div class="icon icon-basket"></div>
          </div>
          <div class="cart-empty-right">
            <div class="cart-empty-title">Ваша корзина пуста</div>
            <div class="cart-empty-text">Добавляйте понравившиеся товары в корзину <br> Или <a href="javascript:void(0)">авторизуйтесь</a>, если добавляли ранее</div>
          </div>
        </div>
      @endif
    </div>
      <div class="catalog">
        <div class="container">
          <div class="catalog-tabs">
            <div class="catalog-tabs-nav">
              <div class="catalog-tabs-nav-title title">Возможно вас заинтересуют эти товары</div>
            </div>
            <div class="catalog-arrows">
              <div class="catalog-arrow catalog-arrow-left">
                <div class="icon icon-arrow-l"></div>
              </div>
              <div class="catalog-arrow catalog-arrow-right">
                <div class="icon icon-arrow-r"></div>
              </div>
            </div>
            <div class="catalog-tabs-list">
              <div class="catalog-tabs-item catalog-tabs-item--1 active">
                <div class="catalog-list catalog-list-4">
                @foreach ($popular as $product)
                  @include('partails.card', ['product' => $product])
                @endforeach
                </div>
                <br>
                <br>
                <br>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>

@endsection