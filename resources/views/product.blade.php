@extends('layouts.app')
@section('title', $product->title )
@section('content')

  <div class="banner_new" style="margin-top: 30px;margin-bottom: 0px;">
    <div class="container">
      <a href="/products"><img src="/media/img/banner_new.png" style="max-width: 100%;" alt=""></a>
    </div>
  </div>
  <div class="page-content page-article">
    <div class="container page-content-row">
      <div class="breadcrumb">
        <ul>
          <li>
            <a href="javascript:void(0)">Главная</a>
          </li>
          <li>
            <a href="/products">Корма</a>
          </li>
          <li>
            <a href="/category/{{$product->category->id}}">{{$product->category->title}}</a>
          </li>
          <li>
            {{$product->title}}
          </li>
        </ul>
      </div>
      <div class="product">
        <div class="product-image">
          <div class="product-image-main">
            <img src="/storage/{{ json_decode($product->images)[0]}}" style="max-width: 100%;" alt="">
          </div>
          <div class="product-image-thumb">
            @if (isset(json_decode($product->images)[0]))
            <div class="product-image-thumb-item"><a href="javascript:void(0)"><img src="{{ json_decode($product->images)[0]}}" alt=""></a></div>
            @endif
            @if (isset(json_decode($product->images)[1]))
            <div class="product-image-thumb-item product-image-thumb-item--video"><a href="javascript:void(0)"><img src="{{ json_decode($product->images)[1]}}" alt=""></a></div>
            @endif
          </div>
        </div>
        <div class="product-info">
          <div class="product-info-top">
            <div class="product-title">
              <div class="product-title-city">{{$product->category->title}}</div>
              <div class="product-title-item">{{$product->title}}</div>
            </div>
          </div>
          <div class="product-tabs">
            <div class="product-tabs-nav">
              <ul>
                <li class="active"><a href="javascript:void(0)" data-tab="2">описание</a></li>
                <!-- <li><a href="javascript:void(0)" data-tab="3">Сертификаты</a></li> -->
              </ul>
            </div>
            <div class="product-tabs-list">
              <div class="product-tabs-item product-tabs-item--2 active">
                <div class="product-description">
                @if (count($attributes))
                <table class="product-table">
                  @foreach($attributes as $attr)
                  <tr>
                    <td>{{$attr->attribute->title}}:</td>
                    <td>{{$attr->attribute_val->attribute_value}}</td>
                  </tr>
                  @endforeach
                </table>
                <br>
                <br>
                @endif
                  {!!$product->text!!}
                </div>
              </div>
              <div class="product-tabs-item product-tabs-item--3">
                <div class="product-cert-list">
                  <ol>
                    <li><a href="javascript:void(0)">Экспертное заключение</a></li>
                    <li><a href="javascript:void(0)">ГОСТ Р Суперфинишная</a></li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="product-actions">
          <div class="product-price">
            {{$product->price}} <span>й</span>
          </div>
          <div class="product-stars">
            <div class="icon icon-star product-star product-star--active"></div>
            <div class="icon icon-star product-star product-star--active"></div>
            <div class="icon icon-star product-star product-star--active"></div>
            <div class="icon icon-star product-star product-star--active"></div>
            <div class="icon icon-star product-star"></div>
          </div>
          <div class="product-stock">
            <i class="icon icon-check-2"></i> <span>есть в наличии</span>
            <!-- <i class="icon icon-x"></i> <span>нет в наличии</span> -->
          </div>
          <form action="javascript:void(0)" method="POST" class="form-cart">
          <div class="product-qty">
            <span class="product-qty-minus"></span><input type="text" value="1" name="qty"><span class="product-qty-plus"></span>
          </div>
          <div class="product-button">
              <a href="javascript:void(0)"><span>В корзину</span><i class="icon icon-plus"></i></a>
              <input type="hidden" name="product" value="{{$product->title}}">
              <input type="hidden" name="price" value="{{$product->price}}">
              <input type="hidden" name="id" value="{{$product->id}}">
              @csrf
          </div>
          </form>
<!--           <div class="product-support">
  <a href="javascript:void(0)"><i class="icon icon-support"></i><span><em>Связаться с</em> <br><em>консультантом</em></span></a>
</div> -->
          <br>
          <br>
        </div>
      </div>
<!--       <div class="product-bottom">
  <div class="product-bottom-item">
    <div class="product-bottom-title">Пищеварение</div>
    <div class="product-bottom-text">Улучшает пищеварение и стабилизирует <br> перистальтику кишечника</div>
  </div>
  <div class="product-bottom-item">
    <div class="product-bottom-title">рН - контроль</div>
    <div class="product-bottom-text">Помогает снизить риск образования камней <br> в мочевыделительной системе</div>
  </div>
  <div class="product-bottom-item">
    <div class="product-bottom-title">Без зерна</div>
    <div class="product-bottom-text">Способствует здоровому пищеварению</div>
  </div>
</div> -->
    </div>
  </div>
  <div class="catalog">
    <div class="container">
      <div class="catalog-tabs">
        <div class="catalog-tabs-nav">
          <div class="catalog-tabs-nav-title title">С этим товаром покупают</div>
        </div>
        <div class="catalog-arrows">
          <div class="catalog-arrow catalog-arrow-left">
            <div class="icon icon-arrow-l"></div>
          </div>
          <div class="catalog-arrow catalog-arrow-right">
            <div class="icon icon-arrow-r"></div>
          </div>
        </div>
        <div class="catalog-tabs-list">
          <div class="catalog-tabs-item catalog-tabs-item--1 active">
            <div class="catalog-list catalog-list-4">
            @foreach ($popular as $product)
              @include('partails.card', ['product' => $product])
            @endforeach
            </div>
            <br>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection