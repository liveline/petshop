@extends('layouts.app')
@section('title', 'Поиск' )
@section('content')

  <div class="page-content page-article">
    <div class="container page-content-row">
      <div class="breadcrumb">
        <ul>
          <li>
            <a href="/">Главная</a>
          </li>
          <li>
            Поиск по запросу {{$_GET['key']}}
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="category">
    <div class="container category-content">
      <div class="category-right" style="width: 100%;">
        <div class="category-title title">По запросу "{{$_GET['key']}}" найдено {{count($products)}} товаров</div>
          <div class="catalog-list catalog-list-category">

            @if (count($products))
              @foreach ($products as $product)
                @include('partails.card', ['product' => $product])
              @endforeach
            @else
              <p>Ничего не найдено</p>
            @endif

          </div>
          <div class="more" style="display: none;">
            <div class="more-title">Показать еще</div>
            <div class="more-icon">
              <div class="icon icon-arrow-down"></div>
            </div>
          </div>
          <br>
          <br>
          <br>
      </div>
    </div>
  </div>

@endsection