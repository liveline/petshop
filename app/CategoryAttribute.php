<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CategoryAttribute extends Model
{
  public function attribute()
  {
      return $this->belongsTo('App\Attribute', 'attribute_id', 'id');
  }
}
