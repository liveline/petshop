<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Attribute extends Model
{
  public function attribute_val()
  {
      return $this->hasMany('App\AttributesValue');
  }
}
