<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use App\Attribute;
use App\Category;
use App\Product;
use App\CategoryAttribute;
use App\ProductAttribute;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;

use TCG\Voyager\Http\Controllers\VoyagerBaseController as BaseVoyagerBaseController;

class VoyagerProductController extends BaseVoyagerBaseController
{
    public function create(Request $request)
    {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('add', app($dataType->model_name));

      $dataTypeContent = (strlen($dataType->model_name) != 0)
                          ? new $dataType->model_name()
                          : false;

      foreach ($dataType->addRows as $key => $row) {
          $details = json_decode($row->details);
          $dataType->addRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
      }

      // If a column has a relationship associated with it, we do not want to show that field
      $this->removeRelationshipField($dataType, 'add');

      // Check if BREAD is Translatable
      $isModelTranslatable = is_bread_translatable($dataTypeContent);

      $view = 'voyager::bread.edit-add';

      if (view()->exists("voyager::$slug.edit-add")) {
          $view = "voyager::$slug.edit-add";
      }

      $attribute = Attribute::all();
      $category = Category::where('parent_id',NULL)->get();

      return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','attribute','category'));
    }

    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $relationships = $this->getRelationships($dataType);

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->with($relationships)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->editRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }
        $attribute = Attribute::all();
        $category = Category::where('parent_id',NULL)->get();

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','attribute','category'));
    }
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {
            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            event(new BreadDataUpdated($dataType, $data));

            $product = Product::find($data->id);
            $product->category_id = $request->category_id;
            $attributes = CategoryAttribute::where('category_id',$request->category_id)->get();
            $ProductAttributes = ProductAttribute::where('product_id',$data->id)->delete();
            foreach ($attributes as $atribute) {
              $productAttr = new ProductAttribute;
              $id = $atribute->attribute_id;
              $name = 'attr-'.$id;
              $productAttr->product_id = $data->id;
              $productAttr->attribute_id = $atribute->attribute_id;
              $productAttr->attribute_value = $request->$name;
              $productAttr->save();
            }

            $product->save();

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }    }

}
