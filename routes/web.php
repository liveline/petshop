<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


Route::get('/', 'MainController@main');
Route::get('/category/{id}', 'MainController@category');
Route::get('/product/{id}', 'MainController@product');
Route::get('/products', 'MainController@products');
Route::get('/delivery', 'MainController@delivery');
Route::get('/pay', 'MainController@pay');
Route::get('/about', 'MainController@about');
Route::get('/cart', 'MainController@cart');
Route::POST('/cart/add', 'MainController@cartAdd');
Route::POST('/cart/remove', 'MainController@cartRemove');
Route::POST('/cart/update', 'MainController@cartUpdate');
Route::get('/page/{url}', 'MainController@page');
Route::POST('/checkout', 'MainController@checkout');
Route::get('/ingredienty_kotorye_my_ispolzuem', 'MainController@ingr_1');
Route::get('/ingredienty_kotorye_my_ne_ispolzuem', 'MainController@ingr_2');
Route::get('/news', 'MainController@news');
Route::get('/news/{id}', 'MainController@news_item');
Route::get('/contacts', 'MainController@contacts');
Route::get('/s', 'MainController@search');
Route::get('/logout', 'MainController@logout');
Route::get('/profile', 'MainController@profile');
Route::get('/cart/get', 'MainController@cartGet');
Route::POST('/registerUser', 'MainController@registerUser');
Route::POST('/subscribe', 'MainController@subscribe');
Route::POST('/order/cartRemove', 'MainController@orderCartRemove');
Route::POST('/order/cartUpdate', 'MainController@orderCartUpdate');
Route::POST('/order/cartAdd', 'MainController@orderCartAdd');
Route::POST('/order/updatePrice', 'MainController@orderUpdatePrice');
Route::POST('/order/updateComment', 'MainController@updateComment');
Route::get('/admin/order/invoice/{id}', 'MainController@invoice');
Route::get('/admin/order/list/{id}', 'MainController@list');
Route::get('/admin/order/path/{id}', 'MainController@path');
Route::get('/admin/order/path/{id}', 'MainController@path');
Route::get('/admin/order/path', 'MainController@path_mass');
Route::get('/admin/order/list', 'MainController@list_mass');
Route::get('/admin/order/invoice', 'MainController@invoice_mass');
Route::post('/admin/order/statusupdate', 'MainController@statusupdate');
Route::post('/admin/order/usercomment', 'MainController@usercomment');
Auth::routes();